import VueRouter from "vue-router";
import Login from "../src/components/Login";
import Signup from "../src/components/Signup";
import HomePage from "@/components/HomePage";

const routes = [
  {
    path: "/",
    name: "login",
    component: Login,
  },
  {
    path: "/signup",
    name: "signup",
    component: Signup,
  },
  {
    path: "/home-page",
    name: "homePage",
    component: HomePage,
  }
];

const router = new VueRouter({
  routes
});

export default router;
