import Vue from 'vue';
import App from './App.vue';
import VueRouter from "vue-router";
import Fragment from 'vue-fragment'
import router from "../router/index";
import 'bootstrap/dist/css/bootstrap.min.css';


Vue.use(VueRouter);
Vue.use(Fragment.Plugin);


Vue.config.productionTip = false;

new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
