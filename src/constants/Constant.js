const TEXT = Object.freeze({
    EMAIL_OR_PASSWORD_DOES_NOT_MATCH: 'Email or password does not match',
    FAILED_TRY_AGAIN: 'Failed try again',
    RESPONSE_ERROR: 'Response error',
    EMAIL_SENT: 'Email sent',
    INVALID_EMAIL: 'Invalid email',
    RECEIVER_AND_MESSAGE_IS_REQUIRED: 'Receiver and message is required',
    BOTH_EMAIL_AND_PASSWORD_IS_REQUIRED: 'Both username and password is required',
    FULL_NAME_EMAIL_AND_PASSWORD_IS_REQUIRED: 'Full name, email and password is required'
});

const CONSTANT = {
    TEXT: TEXT
};

export default CONSTANT;