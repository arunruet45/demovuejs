const bearerToken = '';
const email = '';

const GLOBAL = {
    BEARER_TOKEN: bearerToken,
    USER_EMAIL: email
}

export default GLOBAL;