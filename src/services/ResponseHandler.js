import CONSTANT from "../constants/Constant";

const success = (response) => {
    return {
        data: response?.["data"] || {},
        success: true
    };
};

const error = (responseError) => {
    const {message} = responseError;
    return {
        success: false,
        error: message
    }
};

export {success, error};