import axios from "axios";
import GLOBAL from "@/global";

const axiosInstance = axios.create({
    responseType: 'json',
});

axiosInstance.CancelToken = axios.CancelToken;
axiosInstance.isCancel = axios.isCancel;
axiosInstance.defaults.timeout = 30000;

axiosInstance.interceptors.request.use((config) => {
    const accessToken = localStorage?.accessToken || GLOBAL.BEARER_TOKEN || "";
    config.headers.Authorization = `bearer ${accessToken}`
    return config;

}, (error) => {
    return Promise.reject(error);
});


axiosInstance.interceptors.response.use((response) => {
    return response.data;

}, (error) => {
    return Promise.reject(error);
});

export default axiosInstance;