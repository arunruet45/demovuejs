import axios from "../services/Axios";
import {error, success} from "@/services/ResponseHandler";

const {VUE_APP_BASE_BACKEND_API_URL} = process.env;

const LOGIN_URL = VUE_APP_BASE_BACKEND_API_URL + '/user/login';
const SIGNUP_URL = VUE_APP_BASE_BACKEND_API_URL + '/user/signup';
const SEND_MAIL = VUE_APP_BASE_BACKEND_API_URL + '/user/sendMail';
const LOGOUT_URL = VUE_APP_BASE_BACKEND_API_URL + '/user/logout';

class AuthService {
    async login(payload) {
        try {
            const response = await axios.post(LOGIN_URL, payload);
            return success(response);

        } catch (err) {
            return error(err);
        }
    }

    async signup(payload) {
        try {
            const response = await axios.post(SIGNUP_URL, payload);
            return success(response);

        } catch (err) {
            return error(err);
        }
    }

    async sendMail(payload) {
        try {
            const response = await axios.post(SEND_MAIL, payload);
            return success(response);

        } catch (err) {
            return error(err);
        }
    }

    async logout(payload) {
        try {
            console.log({payload})
            const response = await axios.delete(LOGOUT_URL, {params: payload});
            return success(response);

        } catch (err) {
            return error(err);
        }
    }
}

export default new AuthService();