# demo-vuejs

## Project setup
```
yarn install
```

#setup .env
```
create a .env file in root folder
intialize backend base url variable like this to run in local environment

VUE_APP_BASE_BACKEND_API_URL = http://localhost:3000

```

### Compiles and hot-reloads for development
```
yarn start
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
